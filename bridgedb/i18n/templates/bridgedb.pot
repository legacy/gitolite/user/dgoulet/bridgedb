msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: 'https://gitlab.torproject.org/tpo/anti-censorship/bridgedb\n"
"POT-Creation-Date: 2022-02-10 12:46+PST\n"
"PO-Revision-Date: 2022-02-10 12:46+PST\n"
"Last-Translator: Transifex Bot <>\n"
"Language-Team: English (http://www.transifex.com/otf/torproject/language/en/)\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: https://bridges.torproject.org/ (content/contents+en.lr:page.title)
msgid "Get Bridges for Tor"
msgstr ""

#: https://bridges.torproject.org/ (content/contents+en.lr:page.section)
#: https://bridges.torproject.org/options/
#: (content/options/contents+en.lr:page.section)
#: https://bridges.torproject.org/captcha/
#: (content/captcha/contents+en.lr:page.section)
#: https://bridges.torproject.org/bridges/
#: (content/bridges/contents+en.lr:page.section)
#: https://bridges.torproject.org/howto/
#: (content/howto/contents+en.lr:page.section)
msgid "home"
msgstr ""

#: https://bridges.torproject.org/ (content/contents+en.lr:page.body)
#: https://bridges.torproject.org/options/
#: (content/options/contents+en.lr:page.body)
#: https://bridges.torproject.org/captcha/
#: (content/captcha/contents+en.lr:page.body)
#: https://bridges.torproject.org/bridges/
#: (content/bridges/contents+en.lr:page.body)
#: https://bridges.torproject.org/howto/
#: (content/howto/contents+en.lr:page.body)
msgid "### What are bridges?"
msgstr ""

#: https://bridges.torproject.org/ (content/contents+en.lr:page.body)
#: https://bridges.torproject.org/options/
#: (content/options/contents+en.lr:page.body)
#: https://bridges.torproject.org/captcha/
#: (content/captcha/contents+en.lr:page.body)
#: https://bridges.torproject.org/bridges/
#: (content/bridges/contents+en.lr:page.body)
#: https://bridges.torproject.org/howto/
#: (content/howto/contents+en.lr:page.body)
msgid ""
"[Bridges](https://tb-manual.torproject.org/bridges/) are Tor relays that "
"help you circumvent censorship."
msgstr ""

#: https://bridges.torproject.org/ (content/contents+en.lr:page.body)
#: https://bridges.torproject.org/options/
#: (content/options/contents+en.lr:page.body)
#: https://bridges.torproject.org/captcha/
#: (content/captcha/contents+en.lr:page.body)
#: https://bridges.torproject.org/bridges/
#: (content/bridges/contents+en.lr:page.body)
#: https://bridges.torproject.org/howto/
#: (content/howto/contents+en.lr:page.body)
msgid "### I need an alternative way of getting bridges!"
msgstr ""

#: https://bridges.torproject.org/ (content/contents+en.lr:page.body)
#: https://bridges.torproject.org/options/
#: (content/options/contents+en.lr:page.body)
#: https://bridges.torproject.org/captcha/
#: (content/captcha/contents+en.lr:page.body)
#: https://bridges.torproject.org/bridges/
#: (content/bridges/contents+en.lr:page.body)
#: https://bridges.torproject.org/howto/
#: (content/howto/contents+en.lr:page.body)
msgid ""
"Another way to get bridges is to send an email to [bridges@torproject.org]"
"(mailto:bridges@torproject.org). Leave the email subject empty and write "
"\"get transport obfs4\" in the email's message body. Please note that you "
"must send the email using an address from one of the following email "
"providers: [Riseup](https://riseup.net/) or [Gmail](https://mail.google."
"com/)."
msgstr ""

#: https://bridges.torproject.org/ (content/contents+en.lr:page.body)
#: https://bridges.torproject.org/options/
#: (content/options/contents+en.lr:page.body)
#: https://bridges.torproject.org/captcha/
#: (content/captcha/contents+en.lr:page.body)
#: https://bridges.torproject.org/bridges/
#: (content/bridges/contents+en.lr:page.body)
#: https://bridges.torproject.org/howto/
#: (content/howto/contents+en.lr:page.body)
msgid "### My bridges don't work! I need help!"
msgstr ""

#: https://bridges.torproject.org/ (content/contents+en.lr:page.body)
#: https://bridges.torproject.org/options/
#: (content/options/contents+en.lr:page.body)
#: https://bridges.torproject.org/captcha/
#: (content/captcha/contents+en.lr:page.body)
#: https://bridges.torproject.org/bridges/
#: (content/bridges/contents+en.lr:page.body)
#: https://bridges.torproject.org/howto/
#: (content/howto/contents+en.lr:page.body)
msgid ""
"If your Tor Browser cannot connect, please take a look at the [Tor Browser "
"Manual](https://tb-manual.torproject.org/circumvention/) and our [Support "
"Portal](https://support.torproject.org/#censorship)."
msgstr ""

#: https://bridges.torproject.org/ (content/contents+en.lr:page.intro)
msgid ""
"### BridgeDb can provide bridges with several types of Pluggable Transports, "
"which can help obfuscate your connections to the Tor Network, making it more "
"difficult for anyone watching your internet traffic to determine that you "
"are using Tor."
msgstr ""

#: https://bridges.torproject.org/options/
#: (content/options/contents+en.lr:page.title)
#: https://bridges.torproject.org/captcha/
#: (content/captcha/contents+en.lr:page.title)
#: https://bridges.torproject.org/bridges/
#: (content/bridges/contents+en.lr:page.title)
#: https://bridges.torproject.org/howto/
#: (content/howto/contents+en.lr:page.title)
msgid "BridgeDB"
msgstr ""

#: lego/templates/footer.html:13 lego/templates/footer.html:22
#: lego/templates/navbar.html:97 templates/footer.html:13
#: templates/footer.html:22 templates/navbar.html:97
msgid "Download Tor Browser"
msgstr ""

#: lego/templates/footer.html:14 templates/footer.html:14
msgid ""
"Download Tor Browser to experience real private browsing without tracking, "
"surveillance, or censorship."
msgstr ""

#: lego/templates/footer.html:35 templates/footer.html:35
msgid "Our mission:"
msgstr ""

#: lego/templates/footer.html:36 templates/footer.html:36
msgid ""
"To advance human rights and freedoms by creating and deploying free and open "
"source anonymity and privacy technologies, supporting their unrestricted "
"availability and use, and furthering their scientific and popular "
"understanding."
msgstr ""

#: lego/templates/footer.html:64 lego/templates/footer.html:66
#: lego/templates/navbar.html:19 lego/templates/navbar.html:21
#: lego/templates/navbar.html:65 templates/footer.html:64
#: templates/footer.html:66 templates/navbar.html:19 templates/navbar.html:21
#: templates/navbar.html:65
msgid "Donate"
msgstr ""

#: lego/templates/footer.html:64 lego/templates/footer.html:66
#: lego/templates/navbar.html:19 lego/templates/navbar.html:21
#: templates/footer.html:64 templates/footer.html:66 templates/navbar.html:19
#: templates/navbar.html:21
msgid "Donate Now"
msgstr ""

#: lego/templates/footer.html:75 templates/footer.html:75
msgid "Subscribe to our Newsletter"
msgstr ""

#: lego/templates/footer.html:76 templates/footer.html:76
msgid "Get monthly updates and opportunities from the Tor Project:"
msgstr ""

#: lego/templates/footer.html:77 templates/footer.html:77
msgid "Sign up"
msgstr ""

#: lego/templates/footer.html:101 templates/footer.html:101
#, python-format
msgid ""
"Trademark, copyright notices, and rules for use by third parties can be "
"found in our %(link_to_faq)s"
msgstr ""

#: lego/templates/navbar.html:26 templates/navbar.html:26
msgid "Menu"
msgstr ""

#: lego/templates/navbar.html:56 templates/navbar.html:56
msgid "About"
msgstr ""

#: lego/templates/navbar.html:57 templates/navbar.html:57
msgid "Documentation"
msgstr ""

#: lego/templates/navbar.html:58 templates/navbar.html:58
msgid "Support"
msgstr ""

#: lego/templates/navbar.html:59 templates/navbar.html:59
msgid "Community"
msgstr ""

#: lego/templates/navbar.html:60 templates/navbar.html:60
msgid "Blog"
msgstr ""

#: lego/templates/navbar.html:61 templates/navbar.html:61
msgid "PrivChat"
msgstr ""

#: lego/templates/navbar.html:62 templates/navbar.html:62
msgid "Jobs"
msgstr ""

#: lego/templates/navbar.html:63 templates/navbar.html:63
msgid "Contact"
msgstr ""

#: lego/templates/navbar.html:64 templates/navbar.html:64
msgid "Press"
msgstr ""

#: lego/templates/search.html:5
msgid "Search"
msgstr ""

#: lego/templates/secure-connections.html:1
msgid ""
"The following visualization shows what information is visible to "
"eavesdroppers with and without Tor Browser and HTTPS encryption:"
msgstr ""

#: lego/templates/secure-connections.html:4
msgid ""
"Click the “Tor” button to see what data is visible to observers when you're "
"using Tor. The button will turn green to indicate that Tor is on."
msgstr ""

#: lego/templates/secure-connections.html:5
msgid ""
"Click the “HTTPS” button to see what data is visible to observers when "
"you're using HTTPS. The button will turn green to indicate that HTTPS is on."
msgstr ""

#: lego/templates/secure-connections.html:6
msgid ""
"When both buttons are green, you see the data that is visible to observers "
"when you are using both tools."
msgstr ""

#: lego/templates/secure-connections.html:7
msgid ""
"When both buttons are grey, you see the data that is visible to observers "
"when you don't use either tool."
msgstr ""

#: lego/templates/secure-connections.html:11
msgid "HTTPS"
msgstr ""

#: lego/templates/secure-connections.html:15
#: lego/templates/secure-connections.html:65
msgid "Tor"
msgstr ""

#: lego/templates/secure-connections.html:32
msgid "POTENTIALLY VISIBLE DATA"
msgstr ""

#: lego/templates/secure-connections.html:37
msgid "Site.com"
msgstr ""

#: lego/templates/secure-connections.html:40
msgid "The site being visited."
msgstr ""

#: lego/templates/secure-connections.html:44
msgid "user / pw"
msgstr ""

#: lego/templates/secure-connections.html:47
msgid "Username and password used for authentication."
msgstr ""

#: lego/templates/secure-connections.html:51
msgid "data"
msgstr ""

#: lego/templates/secure-connections.html:54
msgid "Data being transmitted."
msgstr ""

#: lego/templates/secure-connections.html:58
msgid "location"
msgstr ""

#: lego/templates/secure-connections.html:61
msgid ""
"Network location of the computer used to visit the website (the public IP "
"address)."
msgstr ""

#: lego/templates/secure-connections.html:68
msgid "Whether or not Tor is being used."
msgstr ""

#: templates/hero-bridge-lines.html:53
msgid ""
"### How to start using your bridges\n"
"\n"
"First, you need to [download Tor Browser](https://www.torproject.org/"
"projects/torbrowser.html). Our Tor Browser User Manual explains how you can "
"add your bridges to Tor Browser. If you are using Windows, Linux, or OS X, "
"[click here](https://tb-manual.torproject.org/bridges/#entering-bridge-"
"addresses) to learn more. If you are using Android, [click here](https://tb-"
"manual.torproject.org/mobile-tor/#circumvention)."
msgstr ""

#: templates/hero-home.html:5 templates/meta.html:6 templates/meta.html:13
msgid ""
"Defend yourself against tracking and surveillance. Circumvent censorship."
msgstr ""

#: templates/homepage.html:12
msgid "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
msgstr ""

#: templates/meta.html:12
msgid "The Tor Project | Privacy & Freedom Online"
msgstr ""

#: templates/meta.html:22
msgid "Tor Project"
msgstr ""

#: templates/options.html:5
msgid ""
"# Get Bridges!\n"
"\n"
"BridgeDB can provide bridges with several types of [Pluggable Transports]"
"(https://support.torproject.org/glossary/#pluggable-transports), which can "
"help obfuscate your connections to the Tor Network, making it more difficult "
"for anyone watching your internet traffic to determine that you are using "
"Tor.\n"
"\n"
"Some bridges with IPv6 addresses are also available, though some Pluggable "
"Transports aren't IPv6 compatible.\n"
"\n"
"Additionally, BridgeDB has plenty of plain-ol'-vanilla bridges — without any "
"Pluggable Transports — which maybe doesn't sound as cool, but they can still "
"help to circumvent internet censorship in many cases."
msgstr ""
